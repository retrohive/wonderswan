#!/usr/bin/env bash
# This script takes a directory of nointro roms to update our repository.

CURDIR=$(pwd)

systemid="45"
systemname="wonderswan"
ext="ws"

DIRECTORY=$1

set -ex
shopt -s nullglob

update_game()
{
  NEWROM=${1}
  OLDROM=${2}
  PACKAGEDIR=$(dirname ${OLDROM})

  #echo PACKAGEDIR=${PACKAGEDIR}
  NEWSHA1=$(sha1sum ${NEWROM} | awk '{print $1'})
  OLDSHA1=$(sha1sum ${OLDROM} | awk '{print $1'})

  #echo "${NEWSHA1} - ${OLDSHA1}"

  if [ "x${NEWSHA1}" == "x${OLDSHA1}" ]; then
    echo "✅ ${NEWROM}"
  else
    echo "🔄 ${NEWROM}"
    cp ${NEWROM} ${PACKAGEDIR}/
    cd ${PACKAGEDIR}

    VERSIONFULL=$(grep "pkgver=" PKGBUILD)
    VERSIONUM=$(echo ${VERSIONFULL} | cut -d= -f2)
    ((VERSIONUM=VERSIONUM+1))
    VERSIONFULLNEW="pkgver=${VERSIONUM}"

    sed -i s@${VERSIONFULL}@${VERSIONFULLNEW}@ PKGBUILD

    updpkgsums
    makepkg -cf --nodeps  >/dev/null 2>&1
    cd ${CURDIR}
  fi
}

add_game()
{
  file=${1}
  romname=${file%.*}
  mkdir -p Games/${romname}
  cp ${DIRECTORY}/${file} Games/${romname}/
  cd Games/${romname}
  PKGFILE=(*.zstd)
  if (${PKGFILE[@]}); then
    rompom -r ${file} -s ${systemid} -n ${romname} -p ${romname}
    updpkgsums
    makepkg -cf --nodeps  >/dev/null 2>&1
  fi
  cd ${CURDIR}
}

if [ $# -ne 1 ]; then
  echo Wrong number of args
  exit 1
fi

IFS=$'\n'

for file in $(ls ${1}); do
  REALFILE=${file}

  # Skipping BIOS files
  if [[ ${file} =~ ^\[BIOS\].* ]]; then
    continue
  fi

  # Skipping Virtual Console roms
  if [[ ${file} =~ .*'Virtual Console'.* ]]; then
    continue
  fi

  if [[ ${file} =~ .*'Enhancement Chip'.* ]]; then
    continue
  fi

  #echo ${REALFILE}

  EXISTING=$(find ./Games/ -name "${REALFILE}")
  if [ "x${EXISTING}" == "x" ]; then
    echo "❌ ROM ${REALFILE} does not exist yet!"
    add_game "${REALFILE}"
  else
    update_game "${REALFILE}" "${EXISTING}"
  fi

  rm ${1}/${REALFILE}
done
