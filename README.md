# wonderswan

based on https://archive.org/details/no-intro_romsets (20210302-122721)


ROM not added :
```sh
❯ tree -hs --dirsfirst
.
├── [3.1K]  [BIOS] WonderSwan Boot ROM (Japan) (En).zip
└── [ 70K]  Tenori-on (Japan) (En).zip

0 directories, 2 files
```